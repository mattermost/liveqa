#FROM microsoft/dotnet:aspnetcore-runtime
#MAINTAINER Emmanuel Ormancey "emmanuel.ormancey@cern.ch"
## https://docs.docker.com/engine/examples/dotnetcore/
#
#WORKDIR /app
#COPY LiveQA/bin/Debug/netcoreapp2.1/linux-x64/publish/* /app
#
#ENTRYPOINT ["LiveQA"]
#

FROM microsoft/dotnet
#FROM scratch 
MAINTAINER Emmanuel Ormancey "emmanuel.ormancey@cern.ch"
# Add a previously built app binary to an empty image
# To build the binary:
#COPY LiveQA/bin/Release/netcoreapp2.1/linux-x64/publish/*  /
COPY build/ /app
#RUN chmod 777 /app/LiveQA
#RUN ls -al /app/

# Fix error Access to the path '/proc/1/map_files' is denied
WORKDIR /app
EXPOSE 5000
ENTRYPOINT ["/app/LiveQA", "--urls", "http://0.0.0.0:5000"]

