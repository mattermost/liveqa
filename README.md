﻿# Mattermost LiveQA 
App in REACT + ASP.NET Core, settings taken from environment variables.

### Deploy to OpenShift
Go to http://cern.ch/webservices, create a new PaaS website.
When done, open http://openshift.cern.ch, open the newly created application.

Then select *create from image* and use: 
```gitlab-registry.cern.ch/mattermost/liveqa:latest```
 
Optional: Update automatically the deployment if source image changes:
https://blog.openshift.com/image-streams-faq/#scheduled 
or (unclear is proper method):
Openshift -> Builds  -> Images -> LiveQA -> Edit YAML -> add 
```
      importPolicy:
        scheduled: true
```
 
Then add from OpenShift catalog ```cern-sso-proxy``` to enable SSO in front of it and restrict to the e-group of your choice.

### Settings 
Configure the app entirely via Environment variables, to define in OpenShift (Resources -> Config Maps -> Create Config Map)
Environment variables prefixed by ```LIVEQA_```. 
Multiple string values can be provided, separated by ```;```

|        Variable				| Comment | Sample value |
|-------------------------------|---------|--------------|
| LIVEQA_teamName				| Team name where the channels are. Case sensitive.	| IT-dep |
| LIVEQA_channelsPublic			| Public channels for questions. Must provide the name as on the url in MM, not the display name. Multiple allowed, separated by ; | it-talk-live-chat |
| LIVEQA_channelsPrivate		| Private channels for questions, will be displayed anonymously. Must provide the name as on the url in MM, not the display name. Multiple allowed, separated by ; | it-talk-chat-private |
| LIVEQA_channelsTest			| Public channels for questions. Must provide the name as on the url in MM, not the display name. Multiple allowed, separated by ; | test |
| LIVEQA_endpointFeedsURI		| API Uri for MM channel | https://mattermost.web.cern.ch/api/v4/channels/{0}/posts?page=0 |
| LIVEQA_bearer					| bearer token to access the MM API. Obtain one in MM -> Account Settings -> Security -> Personal access token| some string token |
| LIVEQA_userprofileURI			| Retrieve the author profile in MM | https://mattermost.web.cern.ch/api/v4/users/{0} |
| LIVEQA_userimageURI			| Retrieve the author avatar in MM | https://mattermost.web.cern.ch/api/v4/users/{0}/image |
| LIVEQA_attachedfileURI		| Retrieve the attached file in a post | "https://mattermost.web.cern.ch/api/v4/files/{0}/preview" |
| LIVEQA_userbyusernameURI		| Retrieve the user by username. Used to get the ID of moderators specified below | https://mattermost.web.cern.ch/api/v4/users/username/{0} |
| LIVEQA_teambynameURI			| Retrieve team ID from team name | https://mattermost.web.cern.ch/api/v4/teams/name/{0} |
| LIVEQA_channelbynameURI		| Retrieve channel ID from channel name, needs team id | https://mattermost.web.cern.ch/api/v4/teams/{0}/channels/name/{1} |
| LIVEQA_moderatorLogins		| list of MM usernames separated by ; for moderators of this session, the only ones who can #ok a post | ormancey;bsilvade;ibarrien;mbarroso |
| LIVEQA_moderatorApproveTag	| tag meaning thte post was approved for display| #ok |
| LIVEQA_moderatorBypassTag		| tag meaning this post should move up in the list| #up |
| LIVEQA_moderatorAnonymizeTag	| tag meaning author name should be hidden | #anon |
| LIVEQA_MeetingTitle			| Title displayed top of the screen | IT Department Meeting 25th January |
| LIVEQA_FooterText				| Text displayed on the bottom of the screen| Ask your questions on http://cern.ch/mattermost team: it-dep, channel: it-talk-live-chat |
| LIVEQA_MinimumPostDate		| Scan posts after this starting date only. dd-Mon-yyyy format. | 10-Jan-2019 |

### Summary
- The LiveQA site should only be opened by one client, the one displaying on screen.
- Access restriction should be made on the Openshigt NGINX component in the project

### How to use the MM moderation for IT Talk
Reply with tag(s) to the question to validate it: 
- #ok : to validate a post and insert at end of the list. Any text after the tag will be displayed on the app (target someone, etc.)
- #up : to insert this post right after the current one
- #anon : to anonymize the post by using Moderator(s) as sender's name

Channels:
- Public & Test Channels (IT Talk Live Chat & test): displayed as real poster, unless #anon
- Private channel (IT Talk Chat Private) : displayed as Moderator(s)