using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace LiveQA
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Args: " + args.Count());
            foreach (string a in args)
                Console.WriteLine("\targ: " + a);

            CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                 .ConfigureAppConfiguration((hostingContext, config) =>
                 {
                     // Call additional providers here as needed.
                     // Call AddEnvironmentVariables last if you need to allow environment
                     // variables to override values from other providers.
                     config.AddEnvironmentVariables(prefix: "LIVEQA_");
                 })
                .UseConfiguration(new ConfigurationBuilder().AddCommandLine(args).Build())
                //.UseUrls("http://0.0.0.0:5000")
                .UseStartup<Startup>();
    }
}
