﻿using System;

namespace LiveQA
{
    public class Post
    {
        public string AuthorName { get; set; }
        public string Id { get; set; }
        public string ParentId { get; set; }
        public string Text { get; set; }
        public /*BitmapImage*/string Image { get; set; } // base64 encoded data
        public DateTime Date { get; set; }

        // If post comes from a fully trusted channel, bypass all posts in the list
        public bool Bypass { get; set; }

        /// <summary>
        /// If a moderator approved the post by replying to it with #ok in the text
        /// </summary>
        public bool WasApproved = false;
        /// <summary>
        /// Additional moderation message, such as for whom is the question that will be displayed
        /// </summary>
        public string ModerationMessage { get; set; }

        public bool WasAnswered = false;

        public bool Anonymize = false;

        /// <summary>
        /// Mark as true when post has been 
        /// </summary>
        public bool WasDisplayed = false;

        // Ids of attached files
        public string[] file_ids;
        // Attached files in base64 encoding
        public string[] files_base64 { get; set; }
    }
}
