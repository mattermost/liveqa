﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;

namespace LiveQA
{
    public class Config
    {
        public static Config Current;

        public Config(IConfiguration configuration)
        {
            try { teamName = configuration.GetValue<string>("teamName"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'teamName': " + ex.Message); }

            try { channelsPublic = configuration.GetValue<string>("channelsPublic").Split(new char[] { ';' }); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'channelsPublic': " + ex.Message); }

            try { channelsPrivate = configuration.GetValue<string>("channelsPrivate").Split(new char[] { ';' }); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'channelsPrivate': " + ex.Message); }

            try { channelsTest = configuration.GetValue<string>("channelsTest").Split(new char[] { ';' }); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'channelsTest': " + ex.Message); }

            try { bearer = configuration.GetValue<string>("bearer"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'bearer': " + ex.Message); }

            try { endpointFeedsURI = configuration.GetValue<string>("endpointFeedsURI"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'endpointFeedsURI': " + ex.Message); }

            try { userprofileURI = configuration.GetValue<string>("userprofileURI"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'userprofileURI': " + ex.Message); }

            try { userimageURI = configuration.GetValue<string>("userimageURI"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'userimageURI': " + ex.Message); }

            try { userbyusernameURI = configuration.GetValue<string>("userbyusernameURI"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'userbyusernameURI': " + ex.Message); }

            try { attachedfileURI = configuration.GetValue<string>("attachedfileURI"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'attachedfileURI': " + ex.Message); }

            try { teambynameURI = configuration.GetValue<string>("teambynameURI"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'teambynameURI': " + ex.Message); }

            try { channelbynameURI = configuration.GetValue<string>("channelbynameURI"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'channelbynameURI': " + ex.Message); }

            try { moderatorLogins = configuration.GetValue<string>("moderatorLogins").Split(new char[] { ';' }); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'moderatorLogins': " + ex.Message); }

            try { moderatorApproveTag = configuration.GetValue<string>("moderatorApproveTag"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'moderatorApproveTag': " + ex.Message); }

            try { moderatorBypassTag = configuration.GetValue<string>("moderatorBypassTag"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'moderatorBypassTag': " + ex.Message); }

            try { moderatorAnonymizeTag = configuration.GetValue<string>("moderatorAnonymizeTag"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'moderatorAnonymizeTag': " + ex.Message); }

            try { MeetingTitle = configuration.GetValue<string>("MeetingTitle"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'MeetingTitle': " + ex.Message); }

            try { FooterText = configuration.GetValue<string>("FooterText"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'FooterText': " + ex.Message); }

            try { MinimumPostDate = configuration.GetValue<DateTime>("MinimumPostDate"); }
            catch (Exception ex) { throw new Exception("Error getting Environment Variable 'MinimumPostDate': " + ex.Message); }

            Current = this;
        }

        public string teamName { get; set; }
        public string[] channelsPublic { get; set; }
        public string[] channelsPrivate { get; set; }
        public string[] channelsTest { get; set; }

        // Get the bearer from authorization bearier in account settings
        public string bearer { get; set; }

        public string endpointFeedsURI { get; set; }
        public string userprofileURI { get; set; }
        public string userimageURI { get; set; }
        public string userbyusernameURI { get; set; }
        public string attachedfileURI { get; set; }
        public string teambynameURI { get; set; }
        public string channelbynameURI { get; set; }

        public string[] moderatorLogins { get; set; }

        public string moderatorApproveTag { get; set; }
        public string moderatorBypassTag { get; set; }
        public string moderatorAnonymizeTag { get; set; }

        public string MeetingTitle { get; set; }
        public string FooterText { get; set; }
        public DateTime MinimumPostDate { get; set; }


        
    }
}
