﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace LiveQA
{
    class MMGrabber
    {
        private static object _LockObject = new object();

        private static List<Post> _Posts;
        public List<Post> Posts
        {
            get
            {
                if (_Posts == null)
                    _Posts = new List<Post>();
                return _Posts;
            }
            set { _Posts = value; }
        }

        private static List<string> _PostIDs;
        private List<string> PostIDs
        {
            get
            {
                if (_PostIDs == null)
                    _PostIDs = new List<string>();
                return _PostIDs;
            }
            set { _PostIDs = value; }
        }

        private static List<string> _moderatorIDs;
        private List<string> moderatorIDs
        {
            get
            {
                if (_moderatorIDs == null)
                {
                    _moderatorIDs = new List<string>();
                    foreach (string moderatorLogin in Config.Current.moderatorLogins)
                    {
                        string mid = GetModeratorId(moderatorLogin);
                        if (mid != null)
                            _moderatorIDs.Add(mid);
                    }
                }

                return _moderatorIDs;
            }
        }

        private static string _TeamID;
        private static string TeamID { 
            get
            {
                if (_TeamID == null)
                {
                    lock (_LockObject)
                    {
                        if (_TeamID == null)
                        {
                            TeamID = GetTeamId(Config.Current.teamName);
                            Console.WriteLine("Teamid: " + TeamID + " from name: " + Config.Current.teamName);
                        }
                    }
                }
                return _TeamID;
            }
            set
            {
                _TeamID = value;
            }
        }

        private static List<string> _channelsPublic;
        private List<string> channelsPublic
        {
            get
            {
                if (_channelsPublic == null)
                {
                    lock (_LockObject)
                    {
                        if (_channelsPublic == null)
                        {
                            _channelsPublic = new List<string>();
                            foreach (string channelname in Config.Current.channelsPublic)
                            {
                                _channelsPublic.Add(GetChannelId(TeamID, channelname));
                                Console.WriteLine("Loaded Public channel: " + channelname);
                            }
                        }
                    }
                }
                return _channelsPublic;
            }
            set { _channelsPublic = value; }
        }

        private static List<string> _channelsPrivate;
        private List<string> channelsPrivate
        {
            get
            {
                if (_channelsPrivate == null)
                {
                    lock (_LockObject)
                    {
                        if (_channelsPrivate == null)
                        {
                            _channelsPrivate = new List<string>();
                            foreach (string channelname in Config.Current.channelsPrivate)
                            {
                                _channelsPrivate.Add(GetChannelId(TeamID, channelname));
                                Console.WriteLine("Loaded Private channel: " + channelname);
                            }
                        }
                    }
                }
                return _channelsPrivate;
            }
            set { _channelsPrivate = value; }
        }

        private static List<string> _channelsTest;
        private List<string> channelsTest
        {
            get
            {
                if (_channelsTest == null)
                {
                    lock (_LockObject)
                    {
                        if (_channelsTest == null)
                        {
                            _channelsTest = new List<string>();
                            foreach (string channelname in Config.Current.channelsTest)
                            {
                                _channelsTest.Add(GetChannelId(TeamID, channelname));
                                Console.WriteLine("Loaded Test channel: " + channelname);
                            }
                        }
                    }
                }
                return _channelsTest;
            }
            set { _channelsTest = value; }
        }

        public IEnumerable<Post> ApprovedPosts
        {
            get
            {
                if (Posts != null)
                    return Posts.Where(o => o.WasApproved == true);
                else
                    return null;
            }
        }

        public MMGrabber()
        {
            //PostIDs = new List<string>();
            //Posts = new List<Post>();
            //GetPosts();
        }


        public void GetPosts()
        {
            Console.WriteLine("*** Refreshing feeds, with lock ***");
            lock (_LockObject)
            {
                GetPosts(channelsPublic, false);
                GetPosts(channelsTest, false);
                GetPosts(channelsPrivate, true);
            }
        }

        public void GetPosts(List<string> channels, bool anonymize)
        {
            foreach (string channel in channels)
            {
                try
                {
                    var request = (HttpWebRequest)HttpWebRequest.Create(string.Format(Config.Current.endpointFeedsURI, channel));
                    request.Method = "GET";
                    request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + Config.Current.bearer);

                    string jsondata;
                    using (var response = request.GetResponse())
                    {
                        Stream receiveStream = response.GetResponseStream();
                        StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                        jsondata = readStream.ReadToEnd();

                        response.Close();
                        readStream.Close();
                    }

                    dynamic parsed = JsonConvert.DeserializeObject(jsondata);

                    List<Post> moderationPosts = new List<Post>();

                    //Console.WriteLine(parsed.order);
                    foreach (var item in parsed.posts)
                    {
                        //Console.WriteLine(item.First.id);
                        Post p = new Post();
                        p.Bypass = false;
                        p.WasAnswered = false;
                        p.WasApproved = false;

                        p.Id = item.First.id;
                        p.Text = item.First.message;
                        // Clean message text
                        p.Text = p.Text.Trim(new char[] { '"' }).Trim().Replace("\n", " ").Replace("\r", "").Trim();

                        //long unixDate = item.First.update_at;
                        long unixDate = item.First.create_at;

                        DateTime start = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                        p.Date = start.AddMilliseconds(unixDate).ToLocalTime();

                        // Skip posts older than setting 'MinimumPostDate'
                        if (p.Date < Config.Current.MinimumPostDate)
                        {
                            //Console.WriteLine("Skipped post:" + p.Date.ToString());
                            continue;
                        }

                        p.AuthorName = item.First.user_id;

                        if (item.First.file_ids != null)
                            p.file_ids = item.First.file_ids.ToObject<string[]>();

                        if (!PostIDs.Contains(p.Id) )//&& !string.IsNullOrEmpty(p.Text))
                        {
                            // If this is a moderator post for approval, add it to the separate list
                            //if (Config.Current.moderatorIDs.Contains(p.AuthorName)
                            if (moderatorIDs.Contains(p.AuthorName)
                                             && p.Text.ToLower().Contains(Config.Current.moderatorApproveTag.ToLower()))
                            {
                                p.ParentId = item.First.parent_id;
                                if (anonymize || p.Text.ToLower().Contains(Config.Current.moderatorAnonymizeTag.ToLower()))
                                    p.Anonymize = true;
                                if (p.Text.ToLower().Contains(Config.Current.moderatorBypassTag.ToLower()))
                                    p.Bypass = true;
                                if (!string.IsNullOrEmpty(p.ParentId))
                                {
                                    Console.WriteLine("Adding Moderation Post: " + p.Id + " from: " + p.AuthorName);
                                    moderationPosts.Add(p);
                                }
                            }
                            else
                            {
                                // Update author name
                                p.AuthorName = GetAuthorName(item.First.user_id.ToString());

                                // Get user bitmap
                                p.Image = DownloadUserPicture(string.Format(Config.Current.userimageURI, item.First.user_id.ToString()));

                                Console.WriteLine("Adding Post: " + p.Id + " from: " + p.AuthorName);
                                PostIDs.Add(p.Id);
                                Posts.Add(p);
                            }
                        }
                    }

                    // Check for moderator posts and validate posts and download attached files if needed
                    foreach (Post modpost in moderationPosts)
                    {
                        for (int i = 0; i < Posts.Count; i++)
                        {
                            if (Posts[i].Id == modpost.ParentId)
                            {
                                Posts[i].WasApproved = true;
                                Posts[i].Bypass = modpost.Bypass;
                                // Moderation applied, date of the post is now the date of the moderation
                                Posts[i].Date = modpost.Date;
                                //Console.WriteLine("Approved:" + Posts[i].Text + ((Posts[i].Bypass == true)?" and bypassing":" not bypassing"));
                                if (modpost.Anonymize)
                                {
                                    Posts[i].Anonymize = true;
                                    Posts[i].AuthorName = "Moderator(s)";
                                    Posts[i].Image = GenericAvatar; // DownloadUserPicture(Config.Current.noPictureURI);
                                }

                                // Add reply text, stripped from #tags
                                string tmp = modpost.Text.Trim();
                                tmp = Regex.Replace(tmp, @"#\w+\s*", "").Trim();

                                if (tmp.Length > 2)
                                    tmp = tmp.First().ToString().ToUpper() + tmp.Substring(1);

                                if (!string.IsNullOrEmpty(tmp))
                                    Posts[i].ModerationMessage = tmp;

                                // Load files in base64 if any
                                if (Posts[i].file_ids != null)
                                {
                                    List<string> tmpfile = new List<string>();
                                    foreach (string file_id in Posts[i].file_ids)
                                    {
                                        tmpfile.Add(GetFileThumbnailAsBase64(file_id));
                                    }
                                    Posts[i].files_base64 = tmpfile.ToArray<string>();
                                }

                                break;
                            }
                        }
                    }


                    // Sort list by post date
                    Posts = Posts.OrderByDescending(o => o.Bypass).ThenBy(o => o.Date).ToList();
                    // Sort list by post WasDisplayed, then Bypass then Date
                    //Posts = Posts.OrderByDescending(o => o.WasDisplayed).ThenByDescending(o => o.Bypass).ThenBy(o => o.Date).ToList();

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Failed to get channel " + channel + " error: " + ex.Message);
                }
            }
        }

        private string GetAuthorName(string authorid)
        {
            string ret = null;

            var request = (HttpWebRequest)HttpWebRequest.Create(string.Format(Config.Current.userprofileURI, authorid));

            request.Method = "GET";
            request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + Config.Current.bearer);

            string jsondata;
            using (var response = request.GetResponse())
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                jsondata = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
            }

            dynamic parsed = JsonConvert.DeserializeObject(jsondata);
            ret = parsed.first_name;
            ret += " ";
            ret += parsed.last_name;

            return ret;
        }

        private string DownloadUserPicture(string picUrl)
        {
            string ret;
            try
            {
                WebClient web = new WebClient();
                web.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + Config.Current.bearer);

                //web.Credentials = System.Net.CredentialCache.DefaultCredentials;
                //if(!picUrl.Equals(noPictureUrl))
                //    web.Headers.Add("X-FORMS_BASED_AUTH_ACCEPTED", "f");
                //web.Headers.Add("user-agent", "Mozilla/5.0 (Windows NT; Windows NT 6.2; en-GB) SocialFeed/1.0");

                byte[] b = web.DownloadData(picUrl);
                ret = Convert.ToBase64String(b);
                //MemoryStream mem = new MemoryStream(b);
                //image.DecodePixelHeight = 240;
                //image.BeginInit();
                //image.StreamSource = mem;
                //image.EndInit();
            }
            catch
            {
                return null;
            }
            return ret; // image;
        }

        private string GenericAvatar
        {
            get
            {
                var assembly = Assembly.GetEntryAssembly();
                var resourceStream = assembly.GetManifestResourceStream("LiveQA.Code.PersonPlaceholder.png");
                using (var reader = new BinaryReader(resourceStream))
                {
                    byte[] b = reader.ReadBytes((int)resourceStream.Length);
                    return Convert.ToBase64String(b);
                }
            }
        }

        private string GetModeratorId(string moderatorlogin)
        {
            var request = (HttpWebRequest)HttpWebRequest.Create(string.Format(Config.Current.userbyusernameURI, moderatorlogin));

            request.Method = "GET";
            request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + Config.Current.bearer);

            string jsondata;
            using (var response = request.GetResponse())
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                jsondata = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
            }

            dynamic parsed = JsonConvert.DeserializeObject(jsondata);
            return parsed.id;
        }

        private string GetFileThumbnailAsBase64(string file_id)
        {
            WebClient web = new WebClient();
            web.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + Config.Current.bearer);

            byte[] b = web.DownloadData(string.Format(Config.Current.attachedfileURI, file_id));
            string ret = Convert.ToBase64String(b);

            return ret;
        }


        private static string GetTeamId(string teamname)
        {
            var request = (HttpWebRequest)HttpWebRequest.Create(string.Format(Config.Current.teambynameURI, teamname));

            request.Method = "GET";
            request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + Config.Current.bearer);

            string jsondata;
            using (var response = request.GetResponse())
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                jsondata = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
            }

            dynamic parsed = JsonConvert.DeserializeObject(jsondata);
            return parsed.id;
        }

        private string GetChannelId(string teamid, string channelname)
        {
            var request = (HttpWebRequest)HttpWebRequest.Create(string.Format(Config.Current.channelbynameURI, teamid, channelname));

            request.Method = "GET";
            request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + Config.Current.bearer);

            string jsondata;
            using (var response = request.GetResponse())
            {
                Stream receiveStream = response.GetResponseStream();
                StreamReader readStream = new StreamReader(receiveStream, Encoding.UTF8);

                jsondata = readStream.ReadToEnd();

                response.Close();
                readStream.Close();
            }

            dynamic parsed = JsonConvert.DeserializeObject(jsondata);
            return parsed.id;
        }
    }

}
