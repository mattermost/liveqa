import React, { Component } from 'react';
import { Route, Switch } from 'react-router';
import { LiveQA } from './components/LiveQA';
import { Console } from './components/Console';

export default class App extends Component {
  displayName = App.name

  render() {
      return (
          <Switch>
              <Route exact path='/' component={LiveQA} />
              <Route path='/console' component={Console} />
          </Switch>
    );
  }
}
