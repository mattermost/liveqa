import React, { Component } from 'react';
import { Col, Grid, Row } from 'react-bootstrap';
import './LiveQA.css';

export class Console extends Component {
    displayName = Console.name

    constructor() {
        super();
        this.state = {
            currentindex: -1
        };

        this.getCurrentIndex();

        this.keyDownHandler = this.keyDownHandler.bind(this);
        document.addEventListener("keydown", this.keyDownHandler, false);
    }

    keyDownHandler = function (e) {
        var keyCode = e.keyCode;
        if ((keyCode === 37) || (keyCode === 38) || (keyCode === 33)) // Left Up PageUp
        {
            this.indexPreviousHandler();
        }
        else if ((keyCode === 39) || (keyCode === 40) || (keyCode === 34) || (keyCode === 32)) // Right Down PageDown space
        {
            this.indexNextHandler();
        }
    }

    componentDidMount() {
        this.timerIDRemote = setInterval(
            () => this.getCurrentIndex(),
            1 * 1000 // 1 sec
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerIDRemote);
    }

    getCurrentIndex() {
        fetch('api/Mattermost/GetCurrentIndex', { credentials: "same-origin" })
            .then(response => response.json())
            .then(data => {
                this.setState({ currentindex: data });
            });
    }

    indexPreviousHandler = (e) => {
        //console.log("previous");
        fetch('api/Mattermost/IncreaseCurrentIndex/-1', { credentials: "same-origin" })
            .then(response => response.json())
            .then(data => {
                //console.log("got: " + data);
                this.setState({ currentindex: data });
            });
    }

    indexNextHandler = (e) => {
        //console.log("next");
        fetch('api/Mattermost/IncreaseCurrentIndex/1', { credentials: "same-origin" })
            .then(response => response.json())
            .then(data => {
                //console.log("got: " + data);
                this.setState({ currentindex: data });
            });
    }

    render() {
        return (
            <div className="Container">
                <Grid fluid>
                    <Row className="">
                        <Col md={12} className="">
                            <h3>LiveQA Remote Control</h3>
                        </Col>
                    </Row>

                    <Row className="">
                        <Col md={12} className="">
                            <button type="button" className="btn btn-link btn-md" onClick={this.indexPreviousHandler}>
                                <span className="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                            </button>
                            <button type="button" className="btn btn-link btn-md" onClick={this.indexNextHandler}>
                                <span className="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                            </button>
                        </Col>
                    </Row>
                    <Row className="">
                        <Col md={12} className="">
                            Current question: {this.state.currentindex + 1}
                        </Col>
                    </Row>

                </Grid>
            </div>
        );
    }
}