import React, { Component } from 'react';
import { Col, Grid, Row } from 'react-bootstrap';
import './LiveQA.css';
//import logofooter from '../images/LogoOutline-00.png'; 
import logofooter from '../images/cernitslidesfooter.png'; 
import cernwithurl from '../images/cernwithurl.png'; 


export class LiveQA extends Component {
    constructor() {
        super();
        this.state = {
            configloading: true,
            configdata: {},
            questionscountloading: true,
            questionscount: 0,
            currentindex: -1,
            questionloading: true,
            question: {}
        };

        fetch('api/Mattermost/GetConfig', { credentials: "same-origin" })
            .then(response => response.json())
            .then(data => {
                this.setState({ configdata: data, configloading: false });
            });

        this.updateQuestions();
        this.keyDownHandler = this.keyDownHandler.bind(this);
        document.addEventListener("keydown", this.keyDownHandler, false);
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.updateQuestions(),
            10 * 1000 // 10 sec
        );

        this.timerIDRemote = setInterval(
            () => this.getCurrentIndex(),
            1 * 1000 // 1 sec
        );
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
        clearInterval(this.timerIDRemote);
    }

    // Get current Index value in case remote was used
    getCurrentIndex() {
        fetch('api/Mattermost/GetCurrentIndex', { credentials: "same-origin" })
            .then(response => response.json())
            .then(data => {
                if (data !== this.state.currentindex) {
                    this.setState({ currentindex: data, questionloading: true });
                    if (data >= 0) {
                        this.displayQuestion(data);
                    }
                }
                //this.setState({ currentindex: data });
            });
    }

    previous() {
        console.log("previous");
        fetch('api/Mattermost/IncreaseCurrentIndex/-1', { credentials: "same-origin" })
            .then(response => response.json())
            .then(data => {
                //console.log("got: " + data);
                //this.setState({ currentindex: data });
                this.setState({ currentindex: data, questionloading: true });
                if (data >= 0) {
                    this.displayQuestion(data);
                }
            });
    }

    next() {
        console.log("next");
        fetch('api/Mattermost/IncreaseCurrentIndex/1', { credentials: "same-origin" })
            .then(response => response.json())
            .then(data => {
                //console.log("got: " + data);
                //this.setState({ currentindex: data });
                this.setState({ currentindex: data, questionloading: true });
                if (data >= 0) {
                    this.displayQuestion(data);
                }
            });
    }

    updateQuestions() {
        fetch('api/Mattermost/GetQuestionsCount', { credentials: "same-origin" })
            .then(response => response.json())
            .then(data => {
                this.setState({ questionscount: data, questionscountloading: false });
            });
    }

    displayQuestion(index) {
        fetch('api/Mattermost/GetQuestion/' + index, { credentials: "same-origin" })
            .then(response => response.json())
            .then(data => {
                this.setState({ question: data, questionloading: false });
            });
    }

    keyDownHandler = function (e) {
        var keyCode = e.keyCode;
        if ((keyCode === 37) || (keyCode === 38) || (keyCode === 33)) // Left Up PageUp
        {
            this.previous();
        }
        else if ((keyCode === 39) || (keyCode === 40) || (keyCode === 34) || (keyCode === 32)) // Right Down PageDown space
        {
            this.next();
        }
    }

    OLDprevious() {
        var cur = this.state.currentindex - 1;
        if (cur >= this.state.questionscount)
            cur = -1;
        if (cur < 0)
            cur = -1;

        this.setState({ currentindex: cur, questionloading: true });
        if (cur >= 0) {
            this.displayQuestion(cur);
        }
    }

    OLDnext() {
        var cur = this.state.currentindex + 1;
        if (cur >= this.state.questionscount)
            cur = -1;
        if (cur < 0)
            cur = -1;

        this.setState({ currentindex: cur, questionloading: true });
        if (cur >= 0) {
            this.displayQuestion(cur);
        }
    }

    render() {
        let title = this.state.configloading
            ? <div className="loader"></div> //<p><em>Loading...</em></p>
            : this.state.configdata.title;
        let footer = this.state.configloading
            ? <div className="loader"></div> //<p><em>Loading...</em></p>
            : this.state.configdata.footer;
        let counter = this.state.questionscountloading
            ? <span>-</span>
            : this.state.questionscount;

        let question = this.state.questionloading
            ? this.renderLoadingQuestion()
            : this.renderQuestion(this.state.question);

        return (
            <div className="Container">
                <Grid fluid>
                    <Row className="topRow">
                        <Col md={12} className="vcenter text-center title">
                            {title}
                        </Col>
                    </Row>

                    {question}

                    <Row className="navigationRow">
                        <Col md={12} className="vcenter text-right">
                            <button type="button" className="btn btn-link btn-md" onClick={this.previous.bind(this)}>
                                <span className="glyphicon glyphicon-menu-left" aria-hidden="true"></span>
                            </button>
                            <button type="button" className="btn btn-link btn-md" onClick={this.next.bind(this)}>
                                <span className="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                            </button>
                        </Col>
                    </Row>

                    <Row className="bottomRow">
                        <Col md={4} className="vcenter"><img src={logofooter} alt="CERN" style={{ marginTop: '5px' }} /></Col>
                        <Col md={6} className="vcenter text-center footer">{ footer }</Col>
                        <Col md={2} className="vcenter text-right footerCount">
                            {this.state.currentindex < 0 ? "-" : this.state.currentindex + 1}/{counter}
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

    renderQuestion(question)
    {
        let images = ((question.files_base64 === null) || (question.files_base64 === undefined))
            ? ""
            : question.files_base64.map((file) =>
                <img src={"data:image/png;base64, " + file} alt="Attachment" className="attachment" />
                )

        return (
            <Row className="middleRow">
                <Col md={2} className="text-right">
                    <img src={"data:image/png;base64, " + question.image} alt="Avatar" className="questionImage" />
                </Col>
                <Col md={10}>
                    <p className="questionAuthor">{question.authorName}</p>
                    <p className="questionModeration">{question.moderationMessage}</p>

                    <p className={ ((question.text.length > 200) ? 'questionTextSmall': 'questionText') + ' text-justify' }>{question.text}</p>

                    {images}
                </Col>
            </Row>
            );
    }

    renderLoadingQuestion() {
        let loading = this.state.currentindex === -1 //Object.entries(this.state.question).length === 0
            ? <div><img src={cernwithurl} alt="CERN" id="cernwithurl" /><div className="questionsWaiting">{this.state.questionscount} questions received</div></div>
            : <div className="loader"></div> //<div><p><em>Loading...</em></p></div>

        return (
            <Row className="middleRow">
                <Col md={12} className="vcenter middleCol text-center">
                    {loading}
                </Col>
            </Row>
        );

    }
}
