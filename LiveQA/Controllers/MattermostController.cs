using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace LiveQA.Controllers
{
    [Route("api/[controller]")]
    public class MattermostController : Controller
    {
        MMGrabber grab = new MMGrabber();
        private static int _currentIndex = -1;

        [HttpGet("[action]")]
        public int GetQuestionsCount()
        {
            int ret = 0;

            // Update question list at each call
            grab.GetPosts();
            ret = grab.ApprovedPosts.Count();

            return ret;
        }

        [HttpGet("[action]/{index}")]
        public Post GetQuestion(int index)
        {
            if ((index < 0) || (index >= grab.ApprovedPosts.Count()))
                return null;
            else
                return grab.ApprovedPosts.ElementAt(index);
        }

        [HttpGet("[action]")]
        public int GetCurrentIndex()
        {
            return _currentIndex;
        }

        [HttpGet("[action]/{indexIncrease}")]
        public int IncreaseCurrentIndex(int indexIncrease)
        {
            _currentIndex += indexIncrease;
            if ((_currentIndex < 0) || (_currentIndex >= grab.ApprovedPosts.Count()))
                _currentIndex = -1;

            return _currentIndex;
        }

        [HttpGet("[action]")]
        public configdata GetConfig()
        {
            configdata ret = new configdata();
            ret.title = Config.Current.MeetingTitle;
            ret.footer = Config.Current.FooterText;
            return ret;
        }

        public class configdata
        {
            public string title { get; set; }
            public string footer { get; set; }
        }

    }
}
